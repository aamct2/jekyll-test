### Jekyll Test

This is a test repository for using the [Jekyll][jekyll] software for generating a static site.

### License

This repository is licensed under the [MIT license][license].

<!-- Links -->

[jekyll]: https://jekyllrb.com
[license]: ./LICENSE
