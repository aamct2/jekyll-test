BUNDLER := $(shell which bundler)
PROSELINT := $(shell which proselint)
RUBY := $(shell which ruby)

# - Build & Set Up

# Builds the blog
build:
	$(BUNDLER) exec jekyll build -d public
.PHONY: build

# Sets up the workspace (typically used after a fresh clone)
# - Installs dependencies, builds the site
setup: dependencies
	$(MAKE) build
.PHONY: setup

# - Lint

# Lints the posts files
lint:
	$(PROSELINT) ./_posts/
.PHONY: lint

# - Run

# Runs the Jekyll server for local testing
run:
	$(BUNDLER) exec jekyll serve -d public
.PHONY: run

# - Dependencies

# Installs the dependencies
dependencies:
ifndef RUBY
	$(error "Couldn't find Ruby installed.")
endif
	@$(MAKE) install-bundler

# Installs bundler (or installs the gems if already installed)
install-bundler:
ifndef BUNDLER
	$(GEM) install bundler
	bundle install
else
	$(BUNDLER) install
endif
